public class Guayabita {
	private Jugador[] jugadores = new Jugador[2];
	private double pote;
	private int valorAnterior;
	private  int valorActual;
	private int turno;
	private double valorInicialPote;
	
	public Guayabita(Jugador[] jugadores, double valorInicialPote) {
		this.jugadores = jugadores;
		valorAnterior=1;
		valorActual=1;
		this.valorInicialPote=valorInicialPote;
		turno=1;
	}
	
	public double getPote() {
		return pote;
	}

	public void setPote(double pote) {
		this.pote = pote;
	}

	public int getValorAnterior() {
		return valorAnterior;
	}

	public void setValorAnterior(int valorAnterior) {
		this.valorAnterior = valorAnterior;
	}

	public int getTurno() {
		return turno;
	}

	public void setTurno(int turno) {
		this.turno = turno;
	}

	public double getValorInicialPote() {
		return valorInicialPote;
	}

	public void setValorInicialPote(double valorPote) {
		this.valorInicialPote = valorPote;
	}

	public void setJugadores(Jugador[] jugadores) {
		this.jugadores = jugadores;
	}

	public void aumentarPote(double monto) {
		this.pote+=monto;
	}
	
	public void reducirPote(double monto) {
		this.pote-=monto;
	}
	
	public Jugador[] getJugadores() {
		return jugadores;
	}
	
	public int getValorActual() {
		return valorActual;
	}
	
	public void setValorActual(int valorActual) {
		this.valorActual = valorActual;
	}
	
	public void setJugadores(Jugador jugador1, Jugador jugador2) {
		this.jugadores[0]=jugador1;
		this.jugadores[1]=jugador2;
	}
	
	public int lanzarDado() {
		int valor=(int) (Math.random()*6)+1;
		valorAnterior=valorActual;
		if(this.turno==1) {
			valorAnterior=valor;
		}
		valorActual=valor;
		return valor;
	}
	
}
