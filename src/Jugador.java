/**<p>
 * Ésta clase permite crear jugadores para el juego guayabita.
 * </p>
 * <strong>Funciones:</strong>
 * <ul>
 * <li><code>aumentarDinero(double monto)</code>
 * <li><code>reducirDinero(double monto)</code>
 * </ul>
 * @author josemiguelhenaocardona
 *
 */


public class Jugador {
	private String nombre;
	private double dinero;
	
	public Jugador(String nombre, double dinero) {
		this.nombre = nombre;
		this.dinero = dinero;
	}
	
	public void aumentarDinero(double monto) {
		this.dinero+=monto;
	}
	
	public void reducirDinero(double monto) {
		this.dinero-=monto;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public double getDinero() {
		return dinero;
	}
	
	public void setDinero(double dinero) {
		this.dinero = dinero;
	}
	
}
