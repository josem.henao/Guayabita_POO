
/**
 * 
 * @author josemiguelhenaocardona
 *
 */
public class Principal {

	public static void main(String[] args) {
		switch (Mostrar.ventanaInicio()) {
		case "Jugar":
			Jugador[] jugadores=Mostrar.pedirJugadores();
			Guayabita guayaba=new Guayabita(jugadores, Mostrar.asignarValorInicialPote());
			jugar(guayaba);
			break;
		case "Ver Instrucciones":
			Mostrar.verInstrucciones();
			Mostrar.ventanaInicio();
			break;
		case "Salir":
			Mostrar.despedida();
			break;
		default:
			break;
		}
	}
	
	public static void jugar(Guayabita guayaba) {
		Jugador[] jugadores=guayaba.getJugadores();
		jugadores[0].reducirDinero(guayaba.getValorInicialPote());
		jugadores[1].reducirDinero(guayaba.getValorInicialPote());
		guayaba.aumentarPote(guayaba.getValorInicialPote()*2);;
		do {
			if(Mostrar.showOptionDialog(null, "Turno de "+jugadores[guayaba.getTurno()%2].getNombre()+"\nDinero: $"+jugadores[guayaba.getTurno()%2].getDinero(),"Apostra",
					Mostrar.DEFAULT_OPTION, Mostrar.QUESTION_MESSAGE, Mostrar.cargarImagen(7), new Object[] {"Lanzar Dado","Salir"}, "Lanzar Dado")==1) {
				break;
			}	
			guayaba.lanzarDado();
			if(guayaba.getValorActual()==1 || guayaba.getValorActual()==6) {
				Mostrar.showMessageDialog(null, jugadores[guayaba.getTurno()%2].getNombre()+" haz perdido el turno", "Turno perdido", Mostrar.INFORMATION_MESSAGE, 
						Mostrar.cargarImagen(guayaba.getValorActual()));
			}else {
				if(Mostrar.apostar(jugadores[guayaba.getTurno()%2],guayaba.getPote(),guayaba.getValorActual())) {
					double apuesta=0;
					boolean option=true;
					while(option) {//Analiza suficiencia de fondos
						apuesta=Mostrar.introducirApuesta(guayaba.getPote());
						if(apuesta<=jugadores[guayaba.getTurno()%2].getDinero() && apuesta<=guayaba.getPote()) {
							option=false;
						}
					}
					guayaba.lanzarDado();
					if(guayaba.getValorAnterior()<guayaba.getValorActual()) {
						jugadores[guayaba.getTurno()%2].aumentarDinero(apuesta);
						guayaba.reducirPote(apuesta);
						Mostrar.showMessageDialog(null, "Haz ganado la apuesta", "Resultado apuesta", Mostrar.INFORMATION_MESSAGE, 
								Mostrar.cargarImagen(guayaba.getValorActual()));
					}else {
						jugadores[guayaba.getTurno()%2].reducirDinero(apuesta);
						guayaba.aumentarPote(apuesta);
						Mostrar.showMessageDialog(null, "Haz perdido la apuesta", "Resultado apuesta", Mostrar.INFORMATION_MESSAGE, 
								Mostrar.cargarImagen(guayaba.getValorActual()));
					}
				}
			}
			guayaba.setTurno(guayaba.getTurno()+1);
		} while (guayaba.getPote()>0 && jugadores[0].getDinero()>0 && jugadores[1].getDinero()>0);
		Mostrar.mostrarResultados(guayaba);
	}
	
}
