import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**La clase Mostrar hereda de la clase JOptionPane, heredando sus métodos,
 * es util esto para hacer uso de los métodos estáticos. se agregan nuevos
 * métodos usados en el juego de la guayabita como algunas ventanas sencillas
 * para pedir el nombre o la cantidad de dinero que una persona quiere tener 
 * en el juego. La función cargarImagen hace mas fácil la manera de cargar
 * imagenes para ser usadas en las ventanas emergentes durante el juego
 * 
 * 
 * @author josemiguelhenaocardona
 *
 */
public class Mostrar extends JOptionPane {
	
	public static void mostrarResultados(Guayabita guayaba) {
		Jugador[] jugadores=guayaba.getJugadores();
		Mostrar.showMessageDialog(null, "Pote=$"+guayaba.getPote()+"\n"+jugadores[0].getNombre()+"=$"+jugadores[0].getDinero()+"\n"+jugadores[1].getNombre()+"=$"+jugadores[1].getDinero(),
				"Resultados", Mostrar.INFORMATION_MESSAGE, Mostrar.cargarImagen(7));
		if (jugadores[0].getDinero()>guayaba.getValorInicialPote() && jugadores[1].getDinero()>guayaba.getValorInicialPote()) {
			if (Mostrar.showOptionDialog(null, "Desea Continuar con el juego", "Salir", Mostrar.YES_NO_OPTION, Mostrar.QUESTION_MESSAGE,
					Mostrar.cargarImagen(7), new Object[] {"Salir","Continuar"}, "Continuar")==1) {
				Principal.jugar(guayaba);
			}
		}else {
			String ganador;
			if(jugadores[0].getDinero()<jugadores[1].getDinero()) {
				ganador=jugadores[1].getNombre();
			}else if(jugadores[0].getDinero()>jugadores[1].getDinero()) {
				ganador=jugadores[0].getNombre();
			}else {
				ganador="nemo";
			}
			Mostrar.mostrarGanador(ganador);
			Mostrar.despedida();
		}
	}
	
	public static void despedida() {
		Mostrar.showMessageDialog(null, "Adios, que la pasen bien","Despedida",Mostrar.INFORMATION_MESSAGE,Mostrar.cargarImagen(8));

	}
	
	public static void mostrarGanador(String ganador) {
		if (ganador.equals("nemo")) {
			Mostrar.showMessageDialog(null, "No hay un ganador", "Ganador", Mostrar.INFORMATION_MESSAGE, Mostrar.cargarImagen(7));;
		}else {
			Mostrar.showMessageDialog(null, "El ganador es "+ganador, "Ganador", Mostrar.INFORMATION_MESSAGE, Mostrar.cargarImagen(7));;
		}
	}
	public static double introducirApuesta(double pote) {
		String value=Mostrar.pedirDatoSimple("valorApuesta", 0,pote);
		return Double.parseDouble(value);
	}
	
	
	public static boolean apostar(Jugador jugador,double pote, int valorActual) {
		boolean opcion =false;
		int option=Mostrar.showConfirmDialog(null, jugador.getNombre()+" desea hacer una apuesta?\nPote: $"+Double.toString(pote)+"\nTu dinero: $"+jugador.getDinero(), 
				"Hacer una apuesta", Mostrar.YES_NO_OPTION, Mostrar.QUESTION_MESSAGE, Mostrar.cargarImagen(valorActual));
		if(option==0) {
			opcion=true;
		}
		return opcion;
	}
	
	public static String pedirDatoSimple(String tipoDato, int jugador, double pote) {
		String dato="";
		switch (tipoDato) {
		case "nombre":
			do {
				dato=(String)Mostrar.showInputDialog(null,"Ingresa el "+tipoDato+" para el jugador "+jugador,"Ingresar "+tipoDato,
						Mostrar.OK_OPTION,Mostrar.cargarImagen(7),null,"Jugador "+jugador);
			} while(dato == null || dato.trim().isEmpty());
			break;
		case "dinero":
			do {//Me aseguro de sólo ingresar un valor correcto
				dato=(String)Mostrar.showInputDialog(null,"Ingresa el "+tipoDato+" para el jugador "+jugador,"Ingresar "+tipoDato,
						Mostrar.OK_OPTION,Mostrar.cargarImagen(7),null,5000);
			} while (revisarValor(dato));
			break;
		case "valorApuesta":
			do {//Me aseguro de sólo ingresar un valor correcto
				dato=(String) Mostrar.showInputDialog(null, "Pote: $"+pote+"\nIngresa el valor a apostar", "Ingresar apuesta",
						Mostrar.OK_OPTION, Mostrar.cargarImagen(7), null, pote);
			} while (revisarValor(dato));
			break;
		default:
			break;
		}
		return dato;
	}
	
	private static boolean revisarValor(String value) {//retorna true si el valor es malo else lo contrario
		boolean condition=false;
		try {
			double valor=Double.parseDouble(value);
		} catch (Exception e) {
			condition=true;
		}
		return condition;
	}
	
	public static double asignarValorInicialPote() {
		String[] opciones= {"100","200","300","500","1000","2000","3000","5000","El carro","La finca"};
		double valorInicial=200;
		String opcion=Mostrar.showInputDialog(null, "Asigna el valor del pote", "Asignar valor de apuesta", Mostrar.QUESTION_MESSAGE,
				Mostrar.cargarImagen(7),opciones, "200").toString();
		if (opcion.equals("El carro") || opcion.equals("La finca")) {
			valorInicial=10000;
		}else {
			valorInicial=Double.parseDouble(opcion);
		}
		return valorInicial;
	}
	
	public static void verInstrucciones() {
		Mostrar.showMessageDialog(null,"El juego de la guayabita es un juego de azar que se juega con un sólo dado y dos jugadores;\n"
				+ "para comenzar cada jugador debe poner un monto inicial de $200 que se asignan al fondo de apuestas llamado \"Pote\"\n"
				+ "Al azar se elige al jugador que comenzará el juego; éste debe lanzar el dado, si obtiene 1 ó 6 pierde el turno,\n"
				+ "en caso contrario puede apostar un monto menor o igual al valor actual del Pote o ceder el turno; si decide apostar,\n"
				+ "debe lanzar de nuevo el dado, si con el dado se obtiene una valor mayor al lanzamiento anterior, este jugador recibe\n"
				+ "del Pote el valor apostado, en caso contrario se suma al Pote el valor apostado. El segundo jugador puede hacer lo\n"
				+ "mismo que se ha indicado con el jugador uno. El juego termina cuando uno de los jugadores se queda sin dinero", 
				"INSTRUCCIONES", Mostrar.INFORMATION_MESSAGE,Mostrar.cargarImagen(7));
	}
	
	public static ImageIcon cargarImagen(int img){
		String nombre="feijoas.jpg";
		
		switch (img) {
		case 1:
			nombre="uno.png";
			break;
		case 2:
			nombre="dos.png";
			break;
		case 3:
			nombre="tres.png";
			break;
		case 4:
			nombre="cuatro.png";
			break;
		case 5:
			nombre="cinco.png";
			break;
		case 6:
			nombre="seis.png";
			break;
		case 7:
			nombre="feijoas.png";
			break;
		case 8:
			nombre="fail.png";
			break;
		default:
			break;
		}
		ImageIcon imagen=new ImageIcon(Principal.class.getResource(nombre));
		return imagen;
	}
	
	public static String ventanaInicio() {
		String[] opciones={"Salir", "Ver Instrucciones","Jugar"};
		int opcion=Mostrar.showOptionDialog(null, "Bienvenido al juego de la Guayabita", "Bienvenido", 1, JOptionPane.QUESTION_MESSAGE, cargarImagen(7), opciones, 1);
		return opciones[opcion];
	}
	
	public static Jugador[] pedirJugadores(){
		Jugador[] jugadores=new Jugador[2];
		for (int i=0;i<2;i++) {
			String nombre=Mostrar.pedirDatoSimple("nombre", i+1,0);
			String dinero = Mostrar.pedirDatoSimple("dinero", i+1,0);
			jugadores[i]=new Jugador(nombre, Double.parseDouble(dinero));
		}
		return jugadores;
	}
}

